package be.haezebrouck.arduino;

import be.haezebrouck.arduino.rxtx.CommPortIdentifierRepository;
import be.haezebrouck.arduino.rxtx.DeviceNotFoundException;
import gnu.io.CommPortIdentifier;

import java.util.Enumeration;

public class ListUSB {

    public static void main(String... args) {
        System.out.println("usb devices:");
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

        while (portEnum.hasMoreElements()) {
            CommPortIdentifier commPortIdentifier = (CommPortIdentifier) portEnum.nextElement();
            System.out.println(commPortIdentifier.getName() + " - type " + commPortIdentifier.getPortType());
        }
        System.out.println();

        try {
            CommPortIdentifier arduinoCommPort = new CommPortIdentifierRepository().autodetectCommPort();
            System.out.println("Arduino detected on " + arduinoCommPort.getName());
        } catch(DeviceNotFoundException exc) {
            System.out.println("Arduino not recognized");
            System.exit(1);
        }
    }
}
