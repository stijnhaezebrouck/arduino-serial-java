package be.haezebrouck.arduino;

import java.io.IOException;

public class ArduinoIOException extends ArduinoException {

    public ArduinoIOException(IOException exc) {
        super(exc);
    }
}
