package be.haezebrouck.arduino;

public class ArduinoException extends RuntimeException {

    public ArduinoException(Exception cause) {
        super(cause.getMessage(), cause);
    }

    public ArduinoException(String message) {
        super(message);
    }
}
