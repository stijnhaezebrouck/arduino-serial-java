package be.haezebrouck.arduino.rxtx;

import be.haezebrouck.arduino.ArduinoException;

public class DeviceNotFoundException extends ArduinoException {
    public DeviceNotFoundException(String identifierString) {
        super("Device not found: " + identifierString);
    }
}
