package be.haezebrouck.arduino.rxtx;

import be.haezebrouck.arduino.ArduinoIOException;
import gnu.io.CommPortIdentifier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toSet;

public class CommPortIdentifierRepository {
    public CommPortIdentifier getCommPortIdentifier(String identifierString) {
        return findOneOf(singleton(identifierString));
    }

    public CommPortIdentifier autodetectCommPort() {
        try (BufferedReader reader = createKnownDeviceListReader()){
            Set<String> candidates = reader.lines().collect(toSet());
            return findOneOf(candidates);
        } catch (IOException exc) {
            throw new ArduinoIOException(exc);
        }
    }

    private BufferedReader createKnownDeviceListReader() {
        return new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("knowndevicelist")));
    }

    private CommPortIdentifier findOneOf(Set<String> deviceNames) {
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
        while (portEnum.hasMoreElements()) {
            CommPortIdentifier commPortIdentifier = (CommPortIdentifier) portEnum.nextElement();
            if (deviceNames.contains(commPortIdentifier.getName())) return commPortIdentifier;
        }
        throw new DeviceNotFoundException(deviceNames.toString());
    }
}
