package be.haezebrouck.arduino;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class SerialArduinoTextDevice {

    private static final int DATA_RATE = 9600;
    private static final int OPEN_TIMEOUT_MS = 2000;

    private SerialPort serialPort;
    private BufferedReader reader;

    private OutputStream outputStream;
    private PrintWriter printWriter;
    private Logger log = LoggerFactory.getLogger(getClass());

    SerialArduinoTextDevice() {}

    public static SerialArduinoTextDeviceBuilder newArduinoTextSerial() {
        return new SerialArduinoTextDeviceBuilder();
    }

    public BufferedReader reader() throws IOException {
        return reader;
    }

    public void waitUntilString(String waitString) {
        try {
            log.debug("waiting for string '" + waitString + "'...");
            String line = reader.readLine();
            log.debug("waiting-read: " + line);
            while (line != null && !line.contains(waitString)) {
                line = reader.readLine();
                log.debug("waiting-read: " + line);
            }
            if (line == null) {
                throw new ArduinoException("end of Arduino inputstream reached");
            }
        } catch (IOException exc) {
            throw new ArduinoIOException(exc);
        }
    }

    public void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException exc) {}
        }
        if (printWriter != null) {
            printWriter.close();
        }
        if (serialPort != null) {
            serialPort.close();
        }
    }

    void openRxtxSerial(CommPortIdentifier commPortId) {
        try {
            serialPort = openSerialPort(commPortId);
            reader = createReader();
            printWriter = createPrintWriter();
        } catch (IOException exc) {
            throw new ArduinoIOException(exc);
        } catch (PortInUseException | UnsupportedCommOperationException e) {
            throw new ArduinoException(e);
        }
    }

    private BufferedReader createReader() throws IOException {
        return new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
    }

    private PrintWriter createPrintWriter() throws IOException {
        outputStream = serialPort.getOutputStream();
        return new PrintWriter(new OutputStreamWriter(outputStream));
    }

    private SerialPort openSerialPort(CommPortIdentifier comportIdentifier) throws PortInUseException, UnsupportedCommOperationException {
        SerialPort serialPort = (SerialPort) comportIdentifier.open(this.getClass().getName(), OPEN_TIMEOUT_MS);

        serialPort.setSerialPortParams(DATA_RATE,
                SerialPort.DATABITS_8,
                SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);

        return serialPort;
    }

    public PrintWriter printWriter() {
        return printWriter;
    }

    public OutputStream outputStream() { return outputStream; }

}
