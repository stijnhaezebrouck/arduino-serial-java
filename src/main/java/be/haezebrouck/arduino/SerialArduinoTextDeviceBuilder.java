package be.haezebrouck.arduino;

import be.haezebrouck.arduino.rxtx.CommPortIdentifierRepository;
import gnu.io.CommPortIdentifier;

public class SerialArduinoTextDeviceBuilder {

    private CommPortIdentifierRepository commPortIdentifierRepository = new CommPortIdentifierRepository();

    private CommPortIdentifier commPort;

    SerialArduinoTextDeviceBuilder() {
    }

    public SerialArduinoTextDevice start() {
        SerialArduinoTextDevice serial = new SerialArduinoTextDevice();
        serial.openRxtxSerial(commPort);
        return serial;
    }

    public SerialArduinoTextDeviceBuilder onDevice(String deviceName) {
        commPort = commPortIdentifierRepository.getCommPortIdentifier(deviceName);
        return this;
    }

    public SerialArduinoTextDeviceBuilder autoDetect() {
        commPort = commPortIdentifierRepository.autodetectCommPort();
        return this;
    }
}
