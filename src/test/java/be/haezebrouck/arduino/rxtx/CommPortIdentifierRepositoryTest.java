package be.haezebrouck.arduino.rxtx;

import gnu.io.CommPortIdentifier;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.assertThat;

public class CommPortIdentifierRepositoryTest {

    private static final String NON_EXISTING_DEVICE = "/dev/tty.nonexisting";

    private CommPortIdentifierRepository repository = new CommPortIdentifierRepository();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void autodetectCommPortIdentifier_whenFound() {
        CommPortIdentifier commPortId = repository.autodetectCommPort();
        assertThat(commPortId).isNotNull();
    }

    @Test
    public void getCommPortIdentifier_whenNotFound() {
        exception.expect(DeviceNotFoundException.class);
        exception.expectMessage(NON_EXISTING_DEVICE);
        repository.getCommPortIdentifier(NON_EXISTING_DEVICE);
    }
}
