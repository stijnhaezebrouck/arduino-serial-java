package be.haezebrouck.arduino;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static be.haezebrouck.arduino.SerialArduinoTextDevice.newArduinoTextSerial;
import static java.lang.Thread.sleep;
import static java.util.concurrent.Executors.newSingleThreadExecutor;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.assertj.core.api.Assertions.assertThat;

public class SerialArduinoTextDeviceTest {

    private static final Pattern ALIVE_PATTERN = Pattern.compile("^alive\\s\\d+");
    private static final Pattern ECHO_REPLY_PATTERN = Pattern.compile("^reply (\\d+) \'(.*)\'$");
    private static final int TIMEOUT_MS = 3000;
    private static final String READY = "ready";

    private SerialArduinoTextDevice serialArduinoTextDevice;

    private ExecutorService executor = newSingleThreadExecutor(runnable -> new Thread(runnable, "arduinoWaitThread"));

    private Logger log = LoggerFactory.getLogger(getClass());

    @Rule
    public ExpectedException exception = ExpectedException.none();


    @Before
    public void createAndStartArduinoTextDevice() {

        serialArduinoTextDevice = newArduinoTextSerial().autoDetect().start();
    }

    @After
    public void closeArduino() {
        serialArduinoTextDevice.close();
    }

    @After
    public void shutdownExecutorThread() throws Exception {
        executor.shutdown();
        assertThat(executor.awaitTermination(TIMEOUT_MS, MILLISECONDS)).isTrue();
    }

    @Test(timeout = TIMEOUT_MS)
    public void waitUntilString_whenStringIsWritten() {
        serialArduinoTextDevice.waitUntilString(READY);
    }

    @Test
    public void waitUntilString_whenStringIsNeverWritten_shouldBlockUntilClosed() throws Exception {
        Future waitingForStringTask = givenTask(waitUntil("never"));
        whenWaiting(TIMEOUT_MS);
        thenNotFinished(waitingForStringTask);

        whenArduinoClosed();
        thenThrowException(waitingForStringTask);
    }

    @Test(timeout = TIMEOUT_MS)
    public void whenReady_shouldReadAlive() throws IOException {
        givenArduinoReady();
        String readAfterReady = serialArduinoTextDevice.reader().readLine();
        assertThat(ALIVE_PATTERN.matcher(readAfterReady).matches()).describedAs(readAfterReady).isTrue();
    }

    @Test(timeout = TIMEOUT_MS)
    public void whenEchoCommand_shouldRespond() throws IOException {
        givenArduinoReady();
        serialArduinoTextDevice.printWriter().println("test data");
        serialArduinoTextDevice.printWriter().flush();

        String line = serialArduinoTextDevice.reader().readLine();
        while (line != null) {
            log.debug("line="+line);
            if (isReplyFor(line, "test data")) {
                return;
            }
            line = serialArduinoTextDevice.reader().readLine();
        }
    }

    private boolean isReplyFor(String line, String data) {
        Matcher matcher = ECHO_REPLY_PATTERN.matcher(line);
        if (! matcher.matches()) {
            return false;
        }
        return matcher.group(2).equals(data);
    }

    private void thenThrowException(Future waitingForStringTask) throws Exception {
        exception.expect(ExecutionException.class);
        waitingForStringTask.get();
    }

    private void whenArduinoClosed() {
        closeArduino();
    }

    private void thenNotFinished(Future stringRead) {
        assertThat(stringRead.isDone()).isFalse();
    }

    private void whenWaiting(int timeoutMs) throws InterruptedException {
        sleep(timeoutMs);
    }

    private Future givenTask(Runnable runnable) {
        return executor.submit(runnable);
    }

    private void givenArduinoReady() {
        serialArduinoTextDevice.waitUntilString(READY);
    }

    private Runnable waitUntil(String stringToWaitFor) {
        return new WaitUntil(stringToWaitFor);
    }

    private class WaitUntil implements Runnable {

        private final String toWaitFor;

        public WaitUntil(String toWaitFor) {
            this.toWaitFor = toWaitFor;
        }

        @Override
        public void run() throws ArduinoIOException {
            serialArduinoTextDevice.waitUntilString(toWaitFor);
        }
    }

}
