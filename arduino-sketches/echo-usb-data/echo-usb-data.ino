/*
 * Test program that:
 * Sends out alive messages ("alive")
 * Replies to any data received ("reply")
 */

const int BAUDRATE=9600;
const int TIME_OUT=100;
const int ALIVE_TIME=500;
const String EOM = "\n";

unsigned long sequenceNo = 1;
unsigned long lastPingTime = millis();



void setup() {
  Serial.begin(BAUDRATE);
  Serial.setTimeout(TIME_OUT);
  
  sendReady();

}

void loop() {
  String dataRead = Serial.readStringUntil('\n');
  
  if (dataRead.length() > 0) {
      reply(dataRead);
  }
  
  if (aliveTimeExpired()) {
    sendAlive();
  }

}

boolean aliveTimeExpired() {
    return millis() > lastPingTime+ALIVE_TIME;
}

void sendMessageWithContent(String messageType, String messageContent) {
  Serial.print(messageType);
  Serial.print(" ");
  Serial.print(sequenceNo++);
  Serial.print(" \'");
  Serial.print(messageContent);
  Serial.print("\'");
  Serial.print(EOM);
}

void sendMessageWithoutContent(String messageType) {
  Serial.print(messageType);
  Serial.print(" ");
  Serial.print(sequenceNo++);
  Serial.print(EOM);
}

void sendMessageWithoutContentOrSequence(String message) {
    Serial.print(message);
    Serial.print(EOM);
}

void reply(String message) {
  sendMessageWithContent("reply", message);
}

void sendAlive() {
  sendMessageWithoutContent("alive");
  lastPingTime = millis();
}

void sendReady() {
  sendMessageWithoutContentOrSequence("ready");
}

